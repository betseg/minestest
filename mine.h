/* Minestest - a simple mine game
 * Copyright (C) 2018  betseg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see  < https://www.gnu.org/licenses/>.
 */

#pragma once
#include <stdbool.h>

// for the field itself
const int MINE = 9;
// for the mask
const int FLAG = 9;
const bool EMPTY = false;

int IS_MINE(int tile) { return tile == MINE; }

struct {
    // the field and the mask
    struct {
        int tile;
        int mask;
    } **field;
    // sizes of the field
    int height;
    int width;
    // mine count of the current field
    int mine_cnt;
} state;

enum exploration {
    EXPLORE_EMPTY_CELLS = 1, // explores all surrounding non-mine cells
    REVEAL_SURROUNDING_CELLS // explores all cells around the current cell
};

const char *cell[12] = {
    // cell labels, 0 to 8
    "\x1b[1;90m0\x1b[0m",
    "\x1b[94m1",
    "\x1b[32m2",
    "\x1b[91m3",
    "\x1b[34m4",
    "\x1b[31m5",
    "\x1b[36m6",
    "\x1b[35m7",
    "\x1b[37m8",
    "\x1b[41;30mO\x1b[0m", // mine
    "\x1b[41;30mP\x1b[0m", // flag
    "\x1b[90m.", // player doesn't know about the cell
};

int play_game(void);
int explore_neighbors(int x, int y, int reason);
int fill_field(void);
void print_field(void);
void parse_args(int argc, char *argv[]);
void setupsig(void);
void handle(int sig);
void help(char *prog, int h);
