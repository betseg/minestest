# minestest

A CLI mine game.

Has every functionality you'd expect from a mine game.

[![asciicast](https://asciinema.org/a/TSNPgCPEN0CFONHyJdkJ63LVX.png)](https://asciinema.org/a/TSNPgCPEN0CFONHyJdkJ63LVX)

![in-game 1](images/ingame1.png)

![in-game 2](images/ingame2.png)

![help](images/help.png)
