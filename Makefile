all:
	gcc mine.c -o mine

clean:
	rm -f mine

debug:
	gcc mine.c -DDEBUG -o mine
