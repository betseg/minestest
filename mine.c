/* Minestest - a simple mine game
 * Copyright (C) 2018  betseg
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see  < https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
// seed functions
#include <time.h>
#include <string.h>
// timing functions
#include <sys/time.h>
// functions to catch signals
#include <signal.h>
#include <unistd.h>
#include "mine.h"

int main(int argc, char *argv[]) {
    // set up the signal handling code
    setupsig();

    int ret = 0;

    parse_args(argc, argv);

    help(argv[0], 0);

    // exit if an error occures while trying to set up the field
    if((ret = fill_field()) != 0) {
        return ret;
    }

    struct timeval begin, end;
    // the time when the game actually starts
    gettimeofday(&begin, NULL);
    ret = play_game();
    // the time after the game ends
    gettimeofday(&end, NULL);

    printf("Game lasted %.2f seconds.\n",
           (double) (end.tv_usec - begin.tv_usec) / 1000000
         + (double) (end.tv_sec  - begin.tv_sec));

    return ret;
}

int play_game(void) {
    int ret = 0;

    print_field();

    while(1) {
        // internal variables for the loop
        int flag, finished, explr_reason;
        char *get_ret;
        // input variables
        int in_x, in_y, in_f;

        char input_buf[99];
        // prompt
        printf("> ");
        get_ret = fgets(input_buf, sizeof(input_buf), stdin);

        // if the user pressed ^D
        if(get_ret == NULL) {
            ret = 1;
            break;
        }

        flag = sscanf(input_buf, "%d %d %d", &in_x, &in_y, &in_f);

        // if the input is out of bounds, skip it
        if(in_x > state.width - 2 || in_y > state.height - 2
        || in_x < 1 || in_y < 1) {
            continue;
        }

        // if the user didn't enter a flag
        if(flag == 2) {
            //set the internal variable to 0
            in_f = 0;
            // increment the mine count variable if the
            // current cell was flagged previously
            if(IS_MINE(state.field[in_y][in_x].mask)) {
                state.mine_cnt++;
            }
        }
        // if the user entered an invalid input
        // just skip it
        else if(flag != 3) {
            continue;
        }
        puts("");

        // if the inputted cell is a mine
        // and the user didn't enter a flag
        // end game
        if(IS_MINE(state.field[in_y][in_x].tile) && in_f == 0) {
            ret = 1;
            break;
        }

        // if the current cell wasn't flagged before
        // and the user flagged now,
        // decrement the mine count variable
        if(in_f && !IS_MINE(state.field[in_y][in_x].mask)) state.mine_cnt--;

        // the reason to call explore_neighbors
        explr_reason = 0;
        // if the current cell is 0, we want to
        // expand into the zero-mine area
        if(state.field[in_y][in_x].tile == EMPTY)
            explr_reason = EXPLORE_EMPTY_CELLS;
        // if the user knows the cell they entered was empty,
        // then they think they know all the adjacent mines.
        // they want to expand to the adjacet 3x3 area.
        else if(state.field[in_y][in_x].mask == !EMPTY)
            explr_reason = REVEAL_SURROUNDING_CELLS;
        // if the exploration reason is set and the user didn't enter a flag,
        // call explore_neighbors with the current cell coordinates
        // and the reason why we want to explore
        if(explr_reason && in_f == 0) {
            if(in_x != 0 && in_y != 0
               && in_x < state.width - 1
               && in_y < state.height - 1)
                ret = explore_neighbors(in_x, in_y, explr_reason);
        }
        if(explr_reason != 2) ret = 0;
        // if ret is set, that means the user entered
        // a known cell with wrong adjacent mines.
        // exit game.
        else if(ret) break;

        // unmask or flag the current cell according to the input
        state.field[in_y][in_x].mask = in_f ? FLAG : !EMPTY;

        // print the field after the operation
        print_field();

        finished = 0;
        for(int y = 0; y < state.height; y++) {
            for(int x = 0; x < state.width; x++) {
                // the variable starts as 0 and increments
                // only if the current cell is masked,
                // i.e. the user doesn't know about it yet
                finished += !state.field[y][x].mask;
            }
        }
        // if that variable is still zero that means the user
        // has unmasked/flagged every cell.
        // but we still have to check if they just randomly put
        // flags everywhere.
        if(finished == state.mine_cnt) {
            puts("Congratulations! You've beaten the game!");
            break;
        }
    }

    // if the user failed, control flow gets here
    if(ret == 1) {
        puts("\nBetter luck next time!");

        // unmask ever cell and print
        for(int y = 0; y < state.height; y++) {
            for(int x = 0; x < state.width; x++) {
                state.field[y][x].mask = !EMPTY;
            }
        }
        print_field();
    }

    // hopefully we won't leak any memory
    for(int i = 0; i < state.height; i++) {
        free(state.field[i]);
    }
    free(state.field);

    return ret;
}

// this function has 2 uses that basically use the same algorithm.
// reason == EXPLORE_EMPTY_CELLS) if the cell player inputted was 0,
// extend that field until we hit a cell which is adjacent to a mine
// reason == REVEAL_SURROUNDING_CELLS) if the player inputs a cell which
// they already know, assume they want to step on every adjacent cell
int explore_neighbors(int x, int y, int reason) {
    // don't remove the flag, if there is one, if we are being
    // called as a recursion from the same function
    // if there is no flag, just remove the mask
    if(state.field[y][x].mask != FLAG)
        state.field[y][x].mask = !EMPTY;

    // counter for adjacent mines
    int cnt = 0;
    // loop the 3x3 adjacent cells
    for(int dy = -1; dy <= 1; dy++) {
        for(int dx = -1; dx <= 1; dx++) {
            // skip the current cell
            if(dy == 0 && dx == 0) continue;
            // why == 1) don't expand if there are mines in adjacent cells
            if(reason == EXPLORE_EMPTY_CELLS) {
                if(IS_MINE(state.field[y + dy][x + dx].tile)) return 1;
            }
            // reason == REVEAL_) count the mines in adjacent cells
            else
                cnt += IS_MINE(state.field[y + dy][x + dx].mask);
        }
    }

    // if the user inputted a cell they certainly
    // know not to be empty
    if(reason == REVEAL_SURROUNDING_CELLS && cnt != 0) {
        for(int dy = -1; dy <= 1; dy++) {
            for(int dx = -1; dx <= 1; dx++) {
                // if there is a mine that was placed wrongly
                if(IS_MINE(state.field[y + dy][x + dx].mask)
                != IS_MINE(state.field[y + dy][x + dx].tile)) {
                    // exit game
                    return 1;
                }
                // if not, do nothing
            }
        }
    }

    // reason == EXPLR_) do this unconditionally
    // reason == REVEAL_) if the number of adjacent mines correct
    if((reason == EXPLORE_EMPTY_CELLS) || (cnt == state.field[y][x].tile)) {
        // loop the adjacent 3x3 block
        for(int dy = -1; dy <= 1; dy++) {
            for(int dx = -1; dx <= 1; dx++) {
                if(reason == EXPLORE_EMPTY_CELLS) {
                    // skip the current cell
                    if(dy == 0 && dx == 0) continue;
                    // expand if there are more empty areas
                    if(state.field[y + dy][x + dx].mask == EMPTY)
                        if(x+dx != 0 && y+dy != 0
                        && x+dx < state.width - 1
                        && y+dy < state.height - 1)
                            explore_neighbors(x + dx, y + dy,
                                              EXPLORE_EMPTY_CELLS);
                }
                // reason == REVEAL_
                else {
                    // skip the current cell
                    if(dy == 0 && dx == 0) continue;
                    // unmask the adjacent cells
                    // while leaving the flags
                    if(state.field[y + dy][x + dx].mask != FLAG)
                        state.field[y + dy][x + dx].mask = !EMPTY;
                    // if there is a filed with no mines nearby,
                    // expand to there
                    if(state.field[y + dy][x + dx].tile == EMPTY)
                        if(x+dx != 0 && y+dy != 0
                        && x+dx < state.width - 1
                        && y+dy < state.height - 1)
                            explore_neighbors(x + dx, y + dy,
                                              EXPLORE_EMPTY_CELLS);
                }
            }
        }
    }

    return 0;
}

int fill_field(void) {
    // be sure that there can be empty cells
    if((state.height - 2) * (state.width - 2) < state.mine_cnt) {
        puts("Too many mines.");
        return 1;
    }

    // allocate our fields
    state.field = malloc(state.height * sizeof(**state.field));

    // let's not segfault
    if(!state.field)
        return 1;

    // allocate every line
    for(int i = 0; i < state.height; i++) {
        state.field[i] = malloc(state.width * sizeof(*state.field[i]));
        if(!state.field[i])
            return 1;
    }

    // fill the mask
    for(int y = 0; y < state.height; y++) {
        for(int x = 0; x < state.width; x++) {
            state.field[y][x].mask = !EMPTY;
        }
    }

#ifndef DEBUG
    for(int y = 1; y < state.height - 1; y++) {
        for(int x = 1; x < state.width - 1; x++) {
            state.field[y][x].mask = EMPTY;
        }
    }
#endif

    // initialize main field
    for(int y = 0; y < state.height; y++) {
        for(int x = 0; x < state.width; x++) {
            state.field[y][x].tile = EMPTY;
        }
    }

#ifndef DEBUG
    srand(time(NULL));
#else
    srand(0);
#endif
    //fill mines
    for(int i = 0; i < state.mine_cnt; i++) {
        int coord_a = (rand() % (state.height-2)) + 1;
        int coord_b = (rand() % (state.width-2))  + 1;
        //don't put mines in the same cell twice
        if(IS_MINE(state.field[coord_a][coord_b].tile)) {
            i--;
            continue;
        }
        state.field[coord_a][coord_b].tile = MINE;
    }

    //fill numbers one by one
    for(int y = 1; y < state.height-1; y++) {
        for(int x = 1; x < state.width-1; x++) {
            // don't put a number if the current cell is a mine
            if(!IS_MINE(state.field[y][x].tile)) {
                // looping the 3x3 adjacent cells
                for(int dy = -1; dy <= 1; dy++) {
                    for(int dx = -1; dx <= 1; dx++) {
                        // skip the current cell
                        if(dy == 0 && dx == 0) continue;
                        state.field[y][x].tile += IS_MINE(
                                            state.field[y + dy][x + dx].tile);
                    }
                }
            }
        }
    }

    return 0;
}

void print_field(void) {
    printf("%d mine%s left.\n", state.mine_cnt,
                                (state.mine_cnt == 1 ? "" : "s"));

    putchar(' '); // pad the x coordinates by 1 character

    // print x coordinates
    for(int x = 1; x < state.width-1; x++) {
        printf("%d", x%10);
    }
    putchar('\n');

    for(int y = 1; y < state.height-1; y++) {
        // reset color sequence and print the y coordinate
        printf("\x1b[0m%d", y%10);
        // print every cell on that y line
        for(int x = 1; x < state.width-1; x++) {
            // if the player knows anything about the cell
            // print it
            if(state.field[y][x].mask != EMPTY) {
                printf("%s",
                state.field[y][x].mask == FLAG
                ? cell[10] : cell[state.field[y][x].tile]);
            }
            // if they don't, just print a dot
            else {
                printf("%s", cell[11]);
            }
        }
        putchar('\n');
    }
    // reset colors after finishing printing
    printf("\x1b[0m\n");
}

void parse_args(int argc, char *argv[]) {
    if(argc > 1) {
        // predefined difficulty levels
        // and calling help
        if(argc == 2) {
            if((strcmp(argv[1], "-h") == 0)
            || (strcmp(argv[1], "--help") == 0)) {
                help(argv[0], 1);
                exit(0);
            }
            else if(strcmp(argv[1], "easy") == 0) {
                state.width    = 8;
                state.height   = 8;
                state.mine_cnt = 10;
            }
            else if(strcmp(argv[1], "medium") == 0) {
                state.width    = 16;
                state.height   = 16;
                state.mine_cnt = 40;
            }
            // "hard" lands here, as well as anything else
            else {
                state.width    = 30;
                state.height   = 16;
                state.mine_cnt = 99;
            }
        }
        // allow player to define their own difficulty levels
        else if(argc == 4) {
            int i;
            i = atoi(argv[1]);
            state.width    = i ? i : 30;
            i = atoi(argv[2]);
            state.height   = i ? i : 16;
            i = atoi(argv[3]);
            state.mine_cnt = i ? i : 99;
        }
        // arguments are invalid
        else {
            help(argv[0], 1);
            exit(1);
        }
    }
    // no arguments
    else {
        state.width    = 30;
        state.height   = 16;
        state.mine_cnt = 99;
    }

    // padding the field so that
    // we have zeroes all around
    state.height += 2;
    state.width  += 2;
}

// setup for the signal handler
// the actual handler is the function below, void handle(int sig)
void setupsig(void) {
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = handle;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);
}

// handling signals and freeing malloc'd areas
// so we won't leak memory
// except a sigkill, then rip
void handle(int sig) {
    printf("Caught signal \"%s\"! Exiting.\n", strsignal(sig));

    for(int i = 0; i < state.height; i++) {
        free(state.field[i]);
    }
    free(state.field);

    exit(1);
}

void help(char *prog, int h) {
    printf("Minestest Copyright (C) 2018 Betül Ünlü\n"
           "This program comes with ABSOLUTELY NO WARRANTY.\n"
           "This is free software, and you are welcome to redistribute it\n"
           "under certain conditions.\n"
           "Run \"%s --help\" for help.\n", prog);

    if(h) {
        printf("\nUsage: %s [difficulty | width height mine_count]\n"
               "Difficulty can be easy, medium, or hard.\n"
               "Input format while playing:\n"
               "a b c\n"
               "a: x coordinate\n"
               "b: y coordinate\n"
               "c: 0 or 1: step on cell or flag cell (default: 0)\n", prog);
    }
}
